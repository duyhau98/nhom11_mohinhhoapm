﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Field_API.Models
{
    public class bookField
    {
        public int idBookedField { get; set; }
        public int idUser { get; set; }
        public int idOwner { get; set; }
        public int state { get; set; }   
        public int idField { get; set; }
        public DateTime date { get; set; }
        public int IdTime { get; set; }
    }
}
