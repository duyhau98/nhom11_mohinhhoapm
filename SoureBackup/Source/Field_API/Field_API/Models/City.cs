﻿using System;
using System.Collections.Generic;

namespace Field_API.Models
{
    public partial class City
    {
        public City()
        {
            District = new HashSet<District>();
        }

        public int IdCity { get; set; }
        public string CName { get; set; }

        public ICollection<District> District { get; set; }
    }
}
