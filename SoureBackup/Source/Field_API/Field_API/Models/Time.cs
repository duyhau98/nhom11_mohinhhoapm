﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Field_API.Models
{
    public class Time
    {
        public int Id { get; set; }

        public string Real_time { get; set; }
    }
}
