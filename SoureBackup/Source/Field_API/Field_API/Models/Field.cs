﻿using System;
using System.Collections.Generic;

namespace Field_API.Models
{
    public partial class Field
    {
        public int FId { get; set; }
        public string FName { get; set; }
        public int FIdOwner { get; set; }
        public string FAddress { get; set; }
        public string FImage { get; set; }
        public string FInfor { get; set; }
        public string FPrice { get; set; }
        public int FState { get; set; }
        public int? IdDistrict { get; set; }

        public District IdDistrictNavigation { get; set; }
    }
}
