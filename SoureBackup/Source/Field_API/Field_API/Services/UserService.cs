﻿using Field_API.Helper;
using Field_API.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Field_API.Services
{
    public interface IUserService
    {
        string Authenticate(string email, string password, List<Account> listUser);
        //IEnumerable<Account> GetAlls();
    }

    public class UserService :IUserService
    {
        private readonly AppSettings _appSettings;
        //private readonly DatabaseContext _context;

        
        public UserService(IOptions<AppSettings> appSettings
            //, DatabaseContext context, IUserService userService
            )
        {
            _appSettings = appSettings.Value;
            //_context = context; 
        }
        public string Authenticate(string email, string password, List<Account> listAccount)
        {
            var account = listAccount.SingleOrDefault(x => x.Email == email && x.Password == password);

            // return null if user not found
            if (account == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("pId", account.PId.ToString()),
                    new Claim("pName", account.PName.ToString()),
                    new Claim("Role", account.Role),
                    new Claim("PhoneNum", account.PhoneNum),
                    new Claim("Adress", account.Address),
                }),
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);



            return tokenHandler.WriteToken(token); 
        }
        //public IEnumerable<Account> GetAlls()
        //{
        //    // return users without passwords
        //    return _context.Account.ToList().Select(x =>
        //    {
        //        x.Password = null;
        //        return x;
        //    });
        //}


    }
}
