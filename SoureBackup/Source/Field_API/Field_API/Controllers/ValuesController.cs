﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Field_API.Common;
using Field_API.Models;
using Field_API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class ValuesController : ControllerBase
    {
        private readonly DatabaseContext _context;
        private IUserService _userService;
        public ValuesController (DatabaseContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {

            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return null;
        }

        // POST api/values
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Post()
        {
            string token = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                token = reader.ReadToEnd();
            }
            Account user = JsonConvert.DeserializeObject<Account>(token);
            string hashPassword = SHA.GenerateSHA512String(user.Password);

            var userLogin = _userService.Authenticate(user.PName, hashPassword, _context.Account.ToList());
            if (userLogin == null)
            {
                return BadRequest(StatusCode(400));
            }

            return Ok(userLogin);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
