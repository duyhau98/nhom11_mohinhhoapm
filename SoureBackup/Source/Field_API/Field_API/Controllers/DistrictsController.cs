﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Field_API.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public DistrictsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Districts
        [HttpGet]
        public IEnumerable<District> GetDistrict()
        {
            return _context.District;
        }

        // GET: api/Districts
        [HttpGet("/api/list-district-in-city-{idCity}")]
        public IEnumerable<District> GetDistrictByCity(int idCity=0)
        {
            return _context.District.Where(x=>x.City==idCity).ToList();
        }

        // GET: api/Districts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDistrict([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var district = await _context.District.FindAsync(id);

            if (district == null)
            {
                return NotFound();
            }

            return Ok(district);
        }

        // PUT: api/Districts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDistrict([FromRoute] int id, [FromBody] District district)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != district.IdDistrict)
            {
                return BadRequest();
            }

            _context.Entry(district).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DistrictExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        [HttpPost("/api/create-district-in-city-{idCity}")]
        public IActionResult CreateDistrict(int? idCity) {
            if (idCity == null)
                return BadRequest();
            var city = _context.City.Find(idCity);
            if (city == null)
                return BadRequest();

            string disTrictJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                disTrictJson = reader.ReadToEnd();
            }
            if (disTrictJson == null)
            {
                return BadRequest(StatusCode(400));
            }
            District disTrict = JsonConvert.DeserializeObject<District>(disTrictJson);

            disTrict.City = city.IdCity;
            _context.District.Add(disTrict);
            _context.SaveChanges();
            return Ok();
        }

        // POST: api/Districts
        [HttpPost]
        public async Task<IActionResult> PostDistrict([FromBody] District district)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.District.Add(district);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDistrict", new { id = district.IdDistrict }, district);
        }

        // DELETE: api/Districts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDistrict([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var district = await _context.District.FindAsync(id);
            if (district == null)
            {
                return NotFound();
            }

            _context.District.Remove(district);
            await _context.SaveChangesAsync();

            return Ok(district);
        }

        private bool DistrictExists(int id)
        {
            return _context.District.Any(e => e.IdDistrict == id);
        }
    }
}