﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Field_API.Models;
using Field_API.Services;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Newtonsoft.Json;
using Field_API.Common;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class AccountsController : ControllerBase
    {
        private readonly DatabaseContext _context;
        private IUserService _userService;
        public class TempAccount
        {
            public int Id { get; set; }
            public string Password { get; set; }
            public string NewPassword { get; set; }
        }
        public class ObjToken
        {
            public string Token { get; set; }
        }

        public AccountsController(DatabaseContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        [HttpGet("/api/all-accounts")]
        public IEnumerable<Account> GetAllAccounts() {
            return _context.Account;
        }

        [HttpGet("/api/change-status-{status}-account-{accountId}")]
        public IActionResult ChangeStatus(int? status, int? accountId)
        {
            if (status == null||accountId==null)
                return BadRequest();
            var account = _context.Account.Find(accountId);
            account.IsValid = (int)status;
            _context.SaveChanges();
            return Ok(account);

        }
        
        [HttpGet("/api/current-user-profile")]
        [Authorize]
        public ActionResult<IEnumerable<string>> Get()
        {
            string accessToken = Request.Headers["Authorization"];
            string[] arrListStr = accessToken.Split(' ');

            var token = arrListStr[1];
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
            string id = tokenS.Payload.ElementAt(0).Value.ToString();
            Account currentUser = _context.Account.Find(int.Parse(id));
            currentUser.Password = null;
            return Ok(currentUser);
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccount([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var account = await _context.Account.FindAsync(id);

            if (account == null)
            {
                return NotFound();
            }

            return Ok(account);
        }

        // PUT: api/Accounts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAccount([FromRoute] int id, [FromBody] Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != account.PId)
            {
                return BadRequest();
            }

            _context.Entry(account).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/login
        [Authorize]
        [AllowAnonymous]
        [HttpPost ("/api/login")]
        public IActionResult PostAccount()
        {
            string accountJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                accountJson = reader.ReadToEnd();
            }
            Account user = JsonConvert.DeserializeObject<Account>(accountJson);
            string hashPassword = SHA.GenerateSHA512String(user.Password);

            var tokenUser = _userService.Authenticate(user.Email, hashPassword, _context.Account.ToList());
            if (tokenUser == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            ObjToken result = new ObjToken();
            result.Token = tokenUser;
            return Ok(result);
        }
                

        //POST: api/register
        [HttpPost ("/api/register")]
        public IActionResult PostRegister()
        {
            string accJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                accJson = reader.ReadToEnd();
            }
            if(accJson == null)
            {
                return BadRequest(StatusCode(400));
            }
            Account acc = JsonConvert.DeserializeObject<Account>(accJson);        
            acc.Password = SHA.GenerateSHA512String(acc.Password);
            acc.IsValid = 1;
            var temp = _context.Account.SingleOrDefault(x => x.Email == acc.Email);
            if (temp != null)
            { 
                return BadRequest(StatusCode(400));
            }

            _context.Account.Add(acc);
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        [HttpPost("/api/updateInfor")]
        public IActionResult PostUpdateInfor()
        {
            string accJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                accJson = reader.ReadToEnd();
            }
            if (accJson == null)
            {
                return BadRequest(StatusCode(400));
            }
            Account acc = JsonConvert.DeserializeObject<Account>(accJson);
            Account newAccount = _context.Account.Find(acc.PId);
            newAccount.PName = acc.PName;
            newAccount.Address = acc.Address;
            newAccount.PhoneNum = acc.PhoneNum;
            _context.SaveChanges();
            var tokenUser = _userService.Authenticate(newAccount.Email, newAccount.Password, _context.Account.ToList());
            if(tokenUser == null)
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
            ObjToken result = new ObjToken();
            result.Token = tokenUser;
            return Ok(result);
        }

        [HttpPost("/api/password")]
        public IActionResult PostUpdatePassword()
        {
            string pass = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                pass = reader.ReadToEnd();
            }
            if (pass == null)
            {
                return BadRequest(StatusCode(400));
            }
            
            TempAccount acc = JsonConvert.DeserializeObject<TempAccount>(pass);
            Account newAccount = _context.Account.Find(acc.Id);


            acc.Password = SHA.GenerateSHA512String(acc.Password);
            if(acc.Password != newAccount.Password)
            {
                return BadRequest(StatusCode(400));
            }
            //Account newPass = _context.Account.Find(acc.PId);
            acc.NewPassword = SHA.GenerateSHA512String(acc.NewPassword);
            newAccount.Password = acc.NewPassword;
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }


        // DELETE: api/Accounts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccount([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var account = await _context.Account.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Account.Remove(account);
            await _context.SaveChangesAsync();

            return Ok(account);
        }

        private bool AccountExists(int id)
        {
            return _context.Account.Any(e => e.PId == id);
        }
    }
}