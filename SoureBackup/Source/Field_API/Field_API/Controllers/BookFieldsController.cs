﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Field_API.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookFieldsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BookFieldsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/BookFields
        [HttpGet]
        public IEnumerable<bookField> GetBookField()
        {
            return _context.bookField;
        }

        // GET: api/BookFields/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBookField([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookField = await _context.bookField.FindAsync(id);

            if (bookField == null)
            {
                return NotFound();
            }

            return Ok(bookField);
        }

        [HttpGet("/api/get-list-booked-fields-by-IdUser-{IdUser}")]
        public IEnumerable<bookField> GetListFieldByIdUser(int idUser = 0)
        {
            var owner = _context.Account.Where(x => x.PId == idUser).FirstOrDefault();
            if (owner != null)
            {
               return _context.bookField.Where(x => x.idUser == idUser).ToList();
            }
            else
            {
                return null;
            }
            
        }

        [HttpGet("/api/get-list-booked-fields-by-IdOwner-{IdOwner}")]
        public IEnumerable<bookField> GetListFieldByIdOwner(int IdOwner = 0)
        {
            var owner = _context.Account.Where(x => x.PId == IdOwner).FirstOrDefault();
            if (owner != null)
            {
                return _context.bookField.Where(x => x.idOwner == IdOwner).ToList();
            }
            else{
                return null;
            }

        }

        //book field
        [HttpPost("/api/book-field")]
        public IActionResult PBookField()
        {
            string fieldJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                fieldJson = reader.ReadToEnd();
            }
            if (fieldJson == null)
            {
                return BadRequest(StatusCode(400));
            }
            bookField newBookField = JsonConvert.DeserializeObject<bookField>(fieldJson);
            _context.bookField.Add(newBookField);
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        // xóa sân
        [HttpPost("/api/delete-contract-{idBookField}")]
        public IActionResult DeleBookField(int?idBookField)
        {
            
            if(idBookField == null)
            {
                return BadRequest();
            }
            var singleContract = _context.bookField.Find(idBookField);            
            _context.bookField.Remove(singleContract);
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        [HttpPost("/api/update-status-book-field")]
        public IActionResult UStatusBookField()
        {
            string statusBook = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                statusBook = reader.ReadToEnd();
            }
            if (statusBook == null)
            {
                return BadRequest(StatusCode(400));
            }
            bookField field = JsonConvert.DeserializeObject<bookField>(statusBook);
            bookField newBookField = _context.bookField.Find(field.idBookedField);
            newBookField.state = field.state;
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        // PUT: api/BookFields/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBookField([FromRoute] int id, [FromBody] bookField bookField)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bookField.idBookedField)
            {
                return BadRequest();
            }

            _context.Entry(bookField).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookFieldExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BookFields
        [HttpPost]
        public async Task<IActionResult> PostBookField([FromBody] bookField bookField)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.bookField.Add(bookField);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBookField", new { id = bookField.idBookedField }, bookField);
        }


        // DELETE: api/BookFields/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBookField([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookField = await _context.bookField.FindAsync(id);
            if (bookField == null)
            {
                return NotFound();
            }

            _context.bookField.Remove(bookField);
            await _context.SaveChangesAsync();

            return Ok(bookField);
        }

        private bool BookFieldExists(int id)
        {
            return _context.bookField.Any(e => e.idBookedField == id);
        }
    }
}