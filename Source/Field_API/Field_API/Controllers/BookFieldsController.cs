﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Field_API.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookFieldsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BookFieldsController(DatabaseContext context)
        {
            _context = context;
        }

        public class DetailContract
        {
            public Field Field { get; set; }
            public Time Time { get; set; }
            public Account Owner { get; set; }
            public Account user { get; set; }
            public int idBookedField { get; set; }
            public int state { get; set; }
            public DateTime date { get; set; }
        }

        // GET: api/BookFields
        [HttpGet]
        public IEnumerable<bookField> GetBookField()
        {
            return _context.bookField;
        }

        // GET: api/BookFields/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBookField([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookField = await _context.bookField.FindAsync(id);

            if (bookField == null)
            {
                return NotFound();
            }

            return Ok(bookField);
        }

        [HttpGet("/api/get-list-booked-fields-by-IdUser-{IdUser}")]
        public IActionResult GetListFieldByIdUser(int? idUser)
        {
            if (idUser == null)
            {
                return BadRequest(StatusCode(400));
            }
            var owner = _context.Account.Where(x => x.PId == idUser).FirstOrDefault();
            var temp = _context.bookField.Where(x => x.idOwner == idUser).ToList();
            List<DetailContract> detailContracts = new List<DetailContract>();
            foreach (var item in _context.bookField.Where(x => x.idUser == idUser).ToList())
            {
                DetailContract detailCT = new DetailContract();
                detailCT.idBookedField = item.idBookedField;
                detailCT.state = item.state;
                detailCT.date = item.date;
                detailCT.Time = _context.Time.Find(item.IdTime);
                detailCT.Owner = _context.Account.Find(item.idOwner);
                detailCT.user = _context.Account.Find(item.idUser);
                detailCT.Field = _context.Field.Find(item.idField);
                detailContracts.Add(detailCT);
            }

            return Ok(detailContracts);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdOwner"></param>
        /// <returns></returns>
        [HttpGet("/api/get-list-booked-fields-by-IdOwner-{IdOwner}")]
        public IActionResult GetListFieldByIdOwner(int? IdOwner)
        {
            if (IdOwner == null)
            {
                return BadRequest(StatusCode(400));
            }
            var owner = _context.Account.Where(x => x.PId == IdOwner).FirstOrDefault();
            var temp = _context.bookField.Where(x => x.idOwner == IdOwner).ToList();
            List<DetailContract> detailContracts = new List<DetailContract>();
            foreach (var item in _context.bookField.Where(x => x.idOwner == IdOwner).ToList())
            {
                DetailContract detailCT = new DetailContract();
                detailCT.idBookedField = item.idBookedField;
                detailCT.state = item.state;
                detailCT.date = item.date;
                detailCT.Time = _context.Time.Find(item.IdTime);
                detailCT.Owner = _context.Account.Find(item.idOwner);
                detailCT.user = _context.Account.Find(item.idUser);
                detailCT.Field = _context.Field.Find(item.idField);
                detailContracts.Add(detailCT);
            }

            return Ok(detailContracts);


            //      public Field Field { get; set; }
            //public Time Time { get; set; }
            //public Account Owner { get; set; }
            //public Account user { get; set; }
            //public int idBookedField { get; set; }
            //public int state { get; set; }
            //public int date { get; set; }
        }

        [HttpGet("/api/accept-bookfield-{idBookField}")]
        public IActionResult AcceptBookField(int? idBookField)
        {
            var bookField = _context.bookField.Find(idBookField);
            if (bookField == null) return BadRequest();
            bookField.state = 0;
            _context.SaveChanges();

            return Ok(bookField);
        }

        [HttpGet("/api/decline-bookfield-{idBookField}")]
        public IActionResult DeclineBookField(int? idBookField)
        {
            var bookField = _context.bookField.Find(idBookField);
            if (bookField == null) return BadRequest();
            bookField.state = 2;
            _context.SaveChanges();

            return Ok(bookField);
        }

        //book field
        [HttpPost("/api/book-field")]
        public IActionResult PBookField()
        {
            string fieldJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                fieldJson = reader.ReadToEnd();
            }
            if (fieldJson == null)
            {
                return BadRequest(StatusCode(400));
            }
            bookField newBookField = JsonConvert.DeserializeObject<bookField>(fieldJson);
            _context.bookField.Add(newBookField);
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        // xóa sân
        [HttpPost("/api/delete-contract-{idBookField}")]
        public IActionResult DeleBookField(int?idBookField)
        {
            
            if(idBookField == null)
            {
                return BadRequest();
            }
            var singleContract = _context.bookField.Find(idBookField);            
            _context.bookField.Remove(singleContract);
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        [HttpPost("/api/update-status-book-field")]
        public IActionResult UStatusBookField()
        {
            string statusBook = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                statusBook = reader.ReadToEnd();
            }
            if (statusBook == null)
            {
                return BadRequest(StatusCode(400));
            }
            bookField field = JsonConvert.DeserializeObject<bookField>(statusBook);
            bookField newBookField = _context.bookField.Find(field.idBookedField);
            newBookField.state = field.state;
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        // PUT: api/BookFields/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBookField([FromRoute] int id, [FromBody] bookField bookField)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bookField.idBookedField)
            {
                return BadRequest();
            }

            _context.Entry(bookField).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookFieldExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BookFields
        [HttpPost]
        public async Task<IActionResult> PostBookField([FromBody] bookField bookField)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.bookField.Add(bookField);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBookField", new { id = bookField.idBookedField }, bookField);
        }


        // DELETE: api/BookFields/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBookField([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bookField = await _context.bookField.FindAsync(id);
            if (bookField == null)
            {
                return NotFound();
            }

            _context.bookField.Remove(bookField);
            await _context.SaveChangesAsync();

            return Ok(bookField);
        }

        private bool BookFieldExists(int id)
        {
            return _context.bookField.Any(e => e.idBookedField == id);
        }
    }
}