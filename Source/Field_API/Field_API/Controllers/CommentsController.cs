﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Field_API.Models;
using Newtonsoft.Json;
using System.Text;
using System.IO;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public CommentsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Comments
        [HttpGet("/api/all-comments")]
        public IEnumerable<Comment> GetComment()
        {
            return _context.Comment;
        }

        [HttpGet("/api/comments-by-field-{idField}")]
        public IActionResult GetCommentByField(int? idField)
        {
            if (idField == null)
                return BadRequest();
            var k = _context.Comment.Where(x => x.IdField == idField).ToList();
            List<CommenView> listComments = new List<CommenView>();
            foreach(var item in _context.Comment.Where(x => x.IdField == idField).ToList())
            {
                CommenView commentView = new CommenView();
                commentView.Id = item.Id;
                commentView.Account = _context.Account.Find(item.IdAccount);
                commentView.Datetime = item.Datetime;
                commentView.Content = item.Content;
                commentView.IdField = item.IdField;
                listComments.Add(commentView);
            }
            return Ok(listComments);
        }
        // GET: api/Comments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetComment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var comment = await _context.Comment.FindAsync(id);

            if (comment == null)
            {
                return NotFound();
            }

            return Ok(comment);
        }

        // PUT: api/Comments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComment([FromRoute] int id, [FromBody] Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comment.Id)
            {
                return BadRequest();
            }

            _context.Entry(comment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CommentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Comments
        [HttpPost("/api/add-comment")]
        public  IActionResult PostComment()
        {
            string commentJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                commentJson = reader.ReadToEnd();
            }
            Comment comment = JsonConvert.DeserializeObject<Comment>(commentJson);
            if (comment == null) return BadRequest();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                _context.Comment.Add(comment);
                _context.SaveChanges();

            }
            catch (Exception ex)
            {

            }

            return CreatedAtAction("GetComment", new { id = comment.Id }, comment);
        }

        // DELETE: api/Comments/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var comment = await _context.Comment.FindAsync(id);
            if (comment == null)
            {
                return NotFound();
            }

            _context.Comment.Remove(comment);
            await _context.SaveChangesAsync();

            return Ok(comment);
        }

        private bool CommentExists(int id)
        {
            return _context.Comment.Any(e => e.Id == id);
        }
    }
    public class CommenView
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Datetime { get; set; }
        public Account Account { get; set; }

        public int IdField { get; set; }
    }
}