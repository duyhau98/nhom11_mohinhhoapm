﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Field_API.Models;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Field_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FieldsController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public FieldsController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Fields
        [HttpGet("/api/all-fields")]
        public IEnumerable<Field> GetField()
        {
            return _context.Field;
        }

        [HttpGet("/api/field-valid")]
        public IEnumerable<Field> GetFieldValid()
        {
            return _context.Field.Where(x=>x.FState==1);
        }

        [HttpGet("/api/search-field-{nameField}")]
        public IEnumerable<Field> GetFieldsByName(string nameField = "")
        {
            return _context.Field.Where(x => x.FName.ToLower().Trim().Contains(nameField.ToLower().Trim()) && x.FState == 1).ToList();
        }

        //List field by district
        [HttpGet("/api/get-list-field-{districtId}")]
        public IEnumerable<Field> GetListFieldByCity(int districtId = 0)
        {
            var disTrict = _context.District.Where(x => x.IdDistrict == districtId).FirstOrDefault();
            if (disTrict != null)
            {
                return _context.Field.Where(x => x.IdDistrict == disTrict.IdDistrict && x.FState == 1).ToList();

            }
            return _context.Field.ToList();
        }

        //List field by IdUser
        [HttpGet("/api/get-fields-by-IdUser-{IdUser}")]
        public IEnumerable<Field> GetListFieldByIdUser(int idUser = 0)
        {
            var owner = _context.Account.Where(x => x.PId == idUser).FirstOrDefault();
            if (owner != null)
            {
                var a = _context.Field.Where(x => x.FIdOwner == owner.PId).ToList();
                return _context.Field.Where(x => x.FIdOwner == owner.PId).ToList();
            }
            return _context.Field.ToList();
        }

        // GET: api/Fields/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetField([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @field = await _context.Field.FindAsync(id);

            if (@field == null)
            {
                return NotFound();
            }

            return Ok(@field);
        }

        [HttpGet]

        // PUT: api/Fields/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutField([FromRoute] int id, [FromBody] Field @field)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != @field.FId)
            {
                return BadRequest();
            }

            _context.Entry(@field).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FieldExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost("/api/update_field")]
        public IActionResult PostUpdateInfor()
        {
            string tempField = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                tempField = reader.ReadToEnd();
            }
            if (tempField == null)
            {
                return BadRequest(StatusCode(400));
            }
            Field field = JsonConvert.DeserializeObject<Field>(tempField);
            var newField = _context.Field.Find(field.FId);
            newField.FAddress = field.FAddress;
            newField.FImage = field.FImage;
            newField.FInfor = field.FInfor;
            newField.FPrice = field.FPrice;
            newField.FState = field.FState;
            newField.IdDistrict = field.IdDistrict;
            newField.FName = field.FName;
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

         [HttpGet("/api/change-status-field-{idField}-{status}")]
        public IActionResult ChangeStatusField(int? idField, int? status)
        {
            Field field = null;
            if(idField!=null)
            {
                field = _context.Field.Find(idField);
            }
            if(field!=null)
            {
                field.FState = (int) status;
                _context.SaveChanges();
                return Ok(field);
            } else
            {
                return BadRequest();
            }
            
        }


        //update status field
        [HttpPost("/api/accept-book-field-byOwner")]
        public IActionResult AcceptBookField()
        {
            string fields = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                fields = reader.ReadToEnd();
            }
            if (fields == null)
            {
                return BadRequest(StatusCode(400));
            }
            Field field = JsonConvert.DeserializeObject<Field>(fields);
            var newField = _context.Field.Find(field.FId);
            if (newField == null)
            {
                return BadRequest(new { message = "Field not exist" });
            }
            newField.FState = field.FState;
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        [HttpPost("/api/register-field")]
        public IActionResult PRegisterField()
        {
            string fieldJson = string.Empty;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                fieldJson = reader.ReadToEnd();
            }
            if (fieldJson == null)
            {
                return BadRequest(StatusCode(400));
            }
            Field newField = JsonConvert.DeserializeObject<Field>(fieldJson);
            _context.Field.Add(newField);
            _context.SaveChanges();
            return Ok(StatusCode(200));
        }

        


        // POST: api/Fields
        [HttpPost]
        public async Task<IActionResult> PostField([FromBody] Field @field)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Field.Add(@field);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction("GetField", new { id = @field.FId }, @field);
        }

        [HttpGet("/api-detail-field-{idField}-date-{date}")]
        public IActionResult DetailFieldByDate(int?idField, DateTime? date)
        {
            if (idField == null && date == null)
                return BadRequest();

            var field = _context.Field.Find(idField);
            DetailField detailField = new DetailField();
            detailField.Field = field;
            List<StatusField> statusFields = new List<StatusField>();

            foreach(var item in _context.Time)
            {
                bool isBook = false;
                StatusField statusField = new StatusField();
                foreach(var bookField in _context.bookField)
                {
                    if(item.Id == bookField.IdTime )
                    {
                        if (DateTime.Compare((DateTime)date, bookField.date) == 0)
                        {
                            isBook = true;
                            statusField.IdTime = item.Id;
                            statusField.Real_time = item.Real_time;
                            statusField.State = bookField.state;
                            statusFields.Add(statusField);
                            break;
                        } 
                    } 
                    
                }
                if(isBook==false)
                {
                    statusField.IdTime = item.Id;
                    statusField.Real_time = item.Real_time;
                    statusField.State = 2;
                    statusFields.Add(statusField);
                }
            }
            detailField.ListStatusField = statusFields;
            return Ok(detailField);
        }

        // DELETE: api/Fields/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteField([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var @field = await _context.Field.FindAsync(id);
            if (@field == null)
            {
                return NotFound();
            }

            _context.Field.Remove(@field);
            await _context.SaveChangesAsync();

            return Ok(@field);
        }

        private bool FieldExists(int id)
        {
            return _context.Field.Any(e => e.FId == id);
        }
    }

    public class DetailField
    {
        public Field Field { get; set; }

        public List<StatusField> ListStatusField { get; set; }
    }

    public class StatusField
    {
        public int IdTime { get; set; }
        public string Real_time { get; set; }

        public int State { get; set; }
    }
}