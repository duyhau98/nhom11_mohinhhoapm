﻿using System;
using System.Collections.Generic;

namespace Field_API.Models
{
    public partial class District
    {
        public District()
        {
            Field = new HashSet<Field>();
        }

        public int IdDistrict { get; set; }
        public string DName { get; set; }
        public int City { get; set; }

        public City CityNavigation { get; set; }
        public ICollection<Field> Field { get; set; }
    }
}
