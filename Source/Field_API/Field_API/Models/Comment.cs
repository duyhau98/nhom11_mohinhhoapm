﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Field_API.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Datetime { get; set; }
        public int IdAccount { get; set; }

        public int IdField { get; set; }
    }
}
