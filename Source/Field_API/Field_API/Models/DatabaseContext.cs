﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Field_API.Models;

namespace Field_API.Models
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<Field> Field { get; set; }
        public virtual DbSet<bookField> bookField { get; internal set; }

        public virtual DbSet<Time> Time { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=remotemysql.com;port=3306;database=YRDdYsW5r7;uid=YRDdYsW5r7;password=x56mlnyOlV");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.PId);

                entity.ToTable("account");

                entity.Property(e => e.PId)
                    .HasColumnName("pID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasColumnType("text");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("text");

                entity.Property(e => e.PName)
                    .HasColumnName("pName")
                    .HasColumnType("text");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasColumnType("text");

                entity.Property(e => e.PhoneNum)
                    .HasColumnName("phoneNum")
                    .HasColumnType("text");

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<bookField>(entity =>
            {
                entity.HasKey(e => e.idBookedField);
                entity.ToTable("bookField");

                entity.Property(e => e.idBookedField)
                    .HasColumnName("idBookedField")
                    .HasColumnType("int(11)");

                entity.Property(e => e.idUser)
                    .HasColumnName("idUser")
                    .HasColumnType("int(11)");

                entity.Property(e => e.idOwner)
                    .HasColumnName("idOwner")
                    .HasColumnType("int(11)");

                entity.Property(e => e.state)
                    .HasColumnName("state")
                    .HasColumnType("int(11)");

                entity.Property(e => e.idField)
                    .HasColumnName("idfield")
                    .HasColumnType("int(11)");
                entity.Property(e => e.date)
                    .HasColumnName("date")
                    .HasColumnType("Date");
                entity.Property(e => e.IdTime)
                    .HasColumnName("idTime")
                    .HasColumnType("int(11)");


            });



            modelBuilder.Entity<City>(entity =>
            {
                entity.HasKey(e => e.IdCity);

                entity.ToTable("city");

                entity.Property(e => e.IdCity)
                    .HasColumnName("idCity")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CName)
                    .IsRequired()
                    .HasColumnName("cName")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.HasKey(e => e.IdDistrict);

                entity.ToTable("district");

                entity.HasIndex(e => e.City)
                    .HasName("FK_District_City");

                entity.Property(e => e.IdDistrict)
                    .HasColumnName("idDistrict")
                    .HasColumnType("int(11)");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DName)
                    .IsRequired()
                    .HasColumnName("dName")
                    .HasColumnType("text");

                entity.HasOne(d => d.CityNavigation)
                    .WithMany(p => p.District)
                    .HasForeignKey(d => d.City)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_District_City");
            });

            modelBuilder.Entity<Field>(entity =>
            {
                entity.HasKey(e => e.FId);

                entity.ToTable("field");

                entity.HasIndex(e => e.IdDistrict)
                    .HasName("FK_Field_District");

                entity.Property(e => e.FId)
                    .HasColumnName("fId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FIdOwner)
                    .HasColumnName("FIdOwner")
                    .HasColumnType("int(11)");

                entity.Property(e => e.FAddress)
                    .HasColumnName("fAddress")
                    .HasColumnType("text");

                entity.Property(e => e.FImage)
                    .HasColumnName("fImage")
                    .HasColumnType("text");

                entity.Property(e => e.FInfor)
                    .HasColumnName("fInfor")
                    .HasColumnType("text");

                entity.Property(e => e.FName)
                    .HasColumnName("fName")
                    .HasColumnType("text");

                entity.Property(e => e.FPrice)
                    .HasColumnName("fPrice")
                    .HasColumnType("text");

                entity.Property(e => e.FState)
                    .HasColumnName("fState")
                    .HasColumnType("int(1)");

                entity.Property(e => e.IdDistrict)
                    .HasColumnName("idDistrict")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdDistrictNavigation)
                    .WithMany(p => p.Field)
                    .HasForeignKey(d => d.IdDistrict)
                    .HasConstraintName("FK_Field_District");
                modelBuilder.Entity<Field>().Ignore(t => t.IdDistrictNavigation);
                base.OnModelCreating(modelBuilder);
            });

            modelBuilder.Entity<Time>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("time");

                entity.Property(e => e.Id)
                    .HasColumnName("idTime")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Real_time)
                    .IsRequired()
                    .HasColumnName("real_time")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("comment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content")
                    .HasColumnType("text");
               entity.Property(e => e.Datetime)
                    .IsRequired()
                    .HasColumnName("datetime")
                    .HasColumnType("DateTime");

               entity.Property(e => e.IdAccount)
                    .IsRequired()
                    .HasColumnName("idAccount")
                    .HasColumnType("int(11)");

              entity.Property(e => e.IdField)
                    .IsRequired()
                    .HasColumnName("idField")
                    .HasColumnType("int(11)");
            });
        }

        public DbSet<Field_API.Models.Comment> Comment { get; set; }
    }
}
