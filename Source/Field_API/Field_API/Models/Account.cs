﻿using System;
using System.Collections.Generic;

namespace Field_API.Models
{
    public partial class Account
    {
        public int PId { get; set; }
        public string PName { get; set; }
        public string PhoneNum { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public int IsValid { get; set; }
        public string Token { get; set; }
    }
}
